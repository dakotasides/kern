/*
	kern.c
	yep, we're going monolithic
	but minisized
*/
#define MAX_KERNEL_SIZE	10*1024*1024	//10mb

void main(){
	
	/* Rather than hiding everything in functions,
		we're going to initialize everything static
		in a sane, comfortable and verbose manner	*/
		
	/* The kernel process table and first process are 
	initialized, the table is created first, and then
	the first process will be loaded.
	
	If you're wondering why we do this, it's in order
		to provide a static interface for dealing 
		with drivers, kernel helpers, etc */
		
	proctable KernelProcessTable;
	/* Adding the kernel process to the table	*/

	device *kp = KernelProcessTable[0];
	kp.pid=0;
	kp.ppid=0;
	kp.timestart=0;	/* fugid	*/
	kp.memstart = 0; /* this will be the static location
								that the bootloader stores the kernel*/
	kp.memend = MAX_KERNEL_SIZE;	/* no bigger, page tables start next*/
	kp.active = 1; /* This won't change, else, panic	*/
	kp.state = "R"; /*	running	*/
	kp.privs = 10;
	kp.name = "Kernel";

	
	
}


/*
	disk.c
	generic disk driver
	change name, values, etc as needed.
	we're currently just going to assume
	a 50mb qemu disk until acpi/bios is
	more mature. if no disk is provided,
	the system will hang in vfs with a 
	kernel-mode shell. this is for debug
	and would not be a feature of the 
	production kernel.
	
*/

struct disk{
	int *maj,*min;
	int head,cyl;
	int size;
}
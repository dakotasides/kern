/*
	dev.c
	kernel internal device management
*/
#include "types.h"
#DEFINE MAX_DEVICES	20


struct Device{
	int major,minor;
	int busid;
	bool chardev;
	process *driver; /* pointer to driver/handler	*/
	string busloc; /* pointer to bus location	*/
	string reportedname;	/* why not	*/
	char *flags; /* pointer to driver in memory, else 0 */
	bool loaded;
}

struct device DeviceList[MAX_DEVICES];

int lastdev = 0;

/* Creating a device	*/
int regDevice(device device){
	/*	pointers, bro, do you even?	*/
	DeviceList[lastdev] = *device;
	kprintf(0, "dev.c: Device %s loaded. ", DeviceList[lastdev].reportedname);
	lastdev++;
	return lastdev-1; //aka the original ID.
}

bool pollDev(int kdevid, bool new, device dev){
	if(new){
			if(regDevice(dev) == lastdev-1){
				kprintf(0, "dev.c: Device %d registered in table.", kdevid);
				return true;
			}
	}else if{DeviceList[kdevid].loaded){
		kprintf()
		return true;
	}else{
		kprintf(0, "dev.c: Deviceid %d somehow lost?", kdevid)
		return false;
	}
	
}
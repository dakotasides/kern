/*
	fs.c
	generic filesystem driver
	change name and compile as needed
	to include support for your filesystem.
	
	Only one of these can be in the direct
	kernel tree, and this one is for the vfs.
	Once the VFS is mounted/created/populated
	the internal disk driver service will 
	kfork() to the filesystem driver and overlay
	the rootfs onto /, but for the time being
	nothing else will exist. After the rootfs
	overlay/mount completes, all jobs will be 
	transferred into userspace via fork()
*/

/* We Default To the xv6 filesystem	*/
#include "fs/xv6fs.h"

